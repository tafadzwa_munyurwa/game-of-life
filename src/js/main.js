import React from "react";
import ReactDOM from "react-dom";
import Board from "./board"

let App = document.getElementById("app");
ReactDOM.render(<Board size={50} />
    , App);