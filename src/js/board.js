import React from "react"
import Cell from "./cell"
import StateStore from "./stateStore"
import ConwayRules from "./conway.rules"

export default class Board extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            board: StateStore.getInitialBoard(props.size),
            isPlaying: false,
            isPaused: false,
            isStopped: true,
            speed: 50
        }

        this.getCellState = this.getCellState.bind(this)
        this.setCellState = this.setCellState.bind(this)
        this.canChangeCellState = this.canChangeCellState.bind(this)

        this.stop = this.stop.bind(this)
        this.play = this.play.bind(this)
        this.pause = this.pause.bind(this)
    }

    render() {
        const size = this.props.size

        return <div>
            {this.renderTable(size)}
            {this.renderButtons()}
        </div>
    }

    renderRows(size) {
        const rows = []

        for (let x = 0; x < size; x++) {

            const row = <tr key={x} data-row-index={x}>
                {this.renderCells(size, x)}
            </tr>
            rows.push(row)
        }
        return rows
    }

    renderCells(size, row) {
        const cells = []

        for (let y = 0; y < size; y++) {
            const cell = <Cell
                key={y}
                cellIndex={y}
                rowIndex={row}
                canChangeCellState={this.canChangeCellState}
                getCellState={this.getCellState}
                setCellState={this.setCellState} />
            cells.push(cell)
        }

        return cells
    }

    canChangeCellState() {
        return this.state.isPaused || this.state.isStopped
    }

    renderTable(size) {
        return <table className="board">
            <tbody className={this.getBoardColorClass()}>
                {this.renderRows(size)}
            </tbody>
        </table>
    }

    getBoardColorClass() {
        if (this.state.isPaused) {
            return "paused"
        }

        if (this.state.isPlaying) {
            return "playing"
        }

        return null
    }

    renderButtons() {
        return <nav>
            <button className="btn-start" onClick={this.play}>Start</button>
            <button className="btn-pause" onClick={this.pause}>Pause</button>
            <button className="btn-stop" onClick={this.stop} >Stop</button>
        </nav>
    }

    pause() {
        if (!this.playIntervalId) {
            return
        }
        clearInterval(this.playIntervalId)
        this.playIntervalId = null

        this.setState({
            isPaused: true
        })
    }

    stop() {
        this.pause()
        this.setState({
            board: StateStore.getInitialBoard(this.props.size),
            isStopped: true,
            isPaused: false,
            isPlaying: false
        })
    }

    play() {
        if (this.playIntervalId) {
            return
        }

        this.setState({
            isPlaying: true,
            isPaused: false,
            isStopped: false
        })

        this.playIntervalId = setInterval(() => {
            const newBoard = ConwayRules.Step(this.state.board)
            this.setState({ board: newBoard })
        }, this.state.speed);
    }

    getCellState(row, col) {
        return StateStore.getState(this.state.board, row, col)
    }

    setCellState(row, col, val) {
        this.state.board[row][col] = val;
        this.setState(this.state)
    }
}