import StateStore from "./stateStore";

export default class ConwayRules {
    static Step(board) {
        const newBoard = []
        for (let rowIndex = 0; rowIndex < board.length; rowIndex++) {
            const row = board[rowIndex];
            const newRow = []
            for (let colIndex = 0; colIndex < row.length; colIndex++) {
                const neighborhood = this.getNeighborhood(board, rowIndex, colIndex)
                const newState = ConwayRules.determineNewState(neighborhood)
                newRow.push(newState)
            }

            newBoard.push(newRow)
        }

        return newBoard
    }

    static determineNewState(neighborhood) {
        let totalLivingNeighbors = 0
        let currentlyLiving = neighborhood[1][1]
        neighborhood[1][1] = false
        for (let rowIndex = 0; rowIndex < neighborhood.length; rowIndex++) {
            const row = neighborhood[rowIndex];
            for (let cellIndex = 0; cellIndex < row.length; cellIndex++) {
                const cell = row[cellIndex];
                if (cell) {
                    totalLivingNeighbors++;
                }
            }
        }

        if (currentlyLiving && (totalLivingNeighbors === 2 || totalLivingNeighbors === 3)) {
            return true
        }

        if (!currentlyLiving && totalLivingNeighbors === 3) {
            return true
        }

        return false
    }

    static getNeighborhood(board, pivotRow, pivotCol) {
        /**
         * [x,x,x,x]
         * [x,x,x,x]
         * [x,x,x,x]
         * [x,x,x,x]
         */

        const lastRow = board.length - 1
        const lastCol = board[0].length - 1

        const topRow = pivotRow === 0 ? lastRow : pivotRow - 1
        const bottomRow = pivotRow === lastRow ? 0 : pivotRow + 1

        const leftCol = pivotCol === 0 ? lastCol : pivotCol - 1
        const rightCol = pivotCol === lastCol ? 0 : pivotCol + 1

        return [
            [board[topRow][leftCol], board[topRow][pivotCol], board[topRow][rightCol]],
            [board[pivotRow][leftCol], board[pivotRow][pivotCol], board[pivotRow][rightCol]],
            [board[bottomRow][leftCol], board[bottomRow][pivotCol], board[bottomRow][rightCol]]
        ]
    }
}