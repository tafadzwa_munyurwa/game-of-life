import React from 'react'

export default class Cell extends React.Component {
    constructor(props) {
        super(props)
        this.simpleToggleState = this.simpleToggleState.bind(this)
    }

    render() {
        return this.renderCell()
    }

    renderCell() {
        const cellIndex = this.props.cellIndex
        const rowIndex = this.props.rowIndex
        const isAlive = this.props.getCellState(rowIndex, cellIndex)
        return <td data-cell-index={cellIndex}
            data-row-index={rowIndex}
            className={isAlive ? "alive" : null}
            onClick={this.simpleToggleState} />
    }

    simpleToggleState() {
        if (!this.props.canChangeCellState()) {
            return
        }

        const row = this.props.rowIndex
        const cell = this.props.cellIndex
        const currentState = this.props.getCellState(row, cell)
        this.props.setCellState(row, cell, !currentState)
    }
}