let instance = null;
export default class StateStore {
    static getInitialBoard(size) {
        var rows = []
        for (let rowIndex = 0; rowIndex < size; rowIndex++) {
            const cells = new Array(size).fill(false)
            rows.push(cells)
        }

        return rows;
    }

    static getState(board, row, col) {
        return board[row][col]
    }

    static setState(board, row, col, value) {
        board[row][col] = value
    }
}